Package.describe({
  name: 'accounts-ui-bootstrap-3-angular-templates',
  version: '0.0.1'
});

Package.onUse(function(api) {
  api.versionsFrom('1.2.1');
  api.use('blaze-html-templates');

  api.addFiles('custom-templates.html');
});
