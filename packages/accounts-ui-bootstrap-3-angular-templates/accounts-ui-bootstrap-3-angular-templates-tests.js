// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by accounts-ui-bootstrap-3-angular-templates.js.
import { name as packageName } from "meteor/accounts-ui-bootstrap-3-angular-templates";

// Write your tests here!
// Here is an example.
Tinytest.add('accounts-ui-bootstrap-3-angular-templates - example', function (test) {
  test.equal(packageName, "accounts-ui-bootstrap-3-angular-templates");
});
