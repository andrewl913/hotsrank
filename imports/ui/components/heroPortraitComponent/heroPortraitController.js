// import templates .. must import an template for every component
import template from './heroPortrait.html';
import styles from './heroPortrait.scss'

//Import any DB Collections here


class HeroPortraitController {
  constructor($scope, $routeParams, $rootScope) {
    var self = this;
    $scope.viewModel(this);
    $scope.isChecked = false;
    $scope.heroCheckCount = 0;


    this.id = $routeParams.id;
    this.compareReady = false;

    this.updateChecked($scope, $rootScope);
  }

  updateChecked($scope, $rootScope) {

    $scope.$watch('isChecked', function(checked) {
      console.log(this.heroCheckCount)

      if(checked) {
        $rootScope.$broadcast('isChecked');
        console.log(checked)
      } else {
        $scope.heroCheckCount -= 1;
        $rootScope.$broadcast('isUnchecked');
      }
      return checked;
    }, true)
  }

  getPortraitImgSrc() {
    let icon = this.hero.icon;
    let iconLength = icon.length

    let iconNamePng = icon.substring(22, iconLength - 3).toLowerCase() + 'png';

    return '/images/heroesDataConverted/storm_ui_glues_draft_portrait' + iconNamePng;
  }

}


export default HeroPortraitController
