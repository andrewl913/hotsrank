// import templates .. must import an template for every component
import template from './battlegrounds.html';
import styles from './battlegrounds.scss';

//Import any DB Collections here


class battlegroundsController {
  constructor($scope, $routeParams) {

    console.log($routeParams.battleground)
    $scope.viewModel(this);

    this.id = $routeParams.id;
    this.parties = 'hey';
    this.thumbnail = [
      {
        "src": "/images/battlegrounds/thumbnail/bg_towers-of-doom.jpg",
        "name": "Towers of Doom",
        "url": "towers-of-doom"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_sky-temple.jpg",
        "name": "Sky Temple",
        "url": "sky-temple"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_infernal-shrines.jpg",
        "name": "Infernal Shrines",
        "url": "infernal-shrines"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_blackhearts-bay.jpg",
        "name": "Blackhearts Bay",
        "url": "blackhearts-bay"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_garden-of-terror.jpg",
        "name": "Garden of Terror",
        "url": "garden-of-terror"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_cursed-hollow.jpg",
        "name": "Cursed Hollow",
        "url": "cursed-hollow"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_dragon-shire.jpg",
        "name": "Dragon Shire",
        "url": "dragon-shire"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_battlefield-of-eternity.jpg",
        "name": "Battlefield of Eternity",
        "url": "battlefield-of-eternity"
      },
      {
        "src": "/images/battlegrounds/thumbnail/bg_tomb-of-the-spider-queen.jpg",
        "name": "Tomb of The Spider Queen",
        "url": "tomb-of-the-spider-queen"
      }
    ];

  }
}


export default battlegroundsController
