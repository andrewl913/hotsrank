// import templates .. must import an template for every component
import template from './heroes.html';
import styles from './heroes.css'

//Import any DB Collections here


class HeroesController {

  constructor($scope, $routeParams, $http) {
    $scope.viewModel(this);
    this.id = $routeParams.id;
    this.parties = 'hey';
    this.getHeroes($http);
    this.selectedRole = ''
    this.searchParams = ''

    // TODO: add functionality to change the button on search find
    this.isFound = false;

    this.currentHero = null;

    this.numHeroesChecked = 0;

    $scope.$on('isChecked', function() {
      this.numHeroesChecked++;

      console.log(this.numHeroesChecked);
    }.bind(this));

    $scope.$on('isUnchecked', function() {
      if(this.numHeroesChecked >= 1) {
        this.numHeroesChecked--;
      }
      console.log(this.numHeroesChecked);
    }.bind(this));



  }

  getHeroes($http) {
    $http({
      method: 'GET',
      url: 'http://heroesjson.com/heroes.json'
    }).then((response) => {
      this.heroes = response.data;

    })
  }

  search(hero) {
    this.currentHero = hero;
    this.isFound = hero.name.toLowerCase().search(this.searchParams.toLowerCase()) !== -1
    return hero.name.toLowerCase().search(this.searchParams.toLowerCase()) !== -1
  }

  isSelectedRole(hero) {
    return (this.selectedRole === '' || hero.role === this.selectedRole)
  }


}


export default HeroesController
