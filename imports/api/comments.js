import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';


export const Comments = new Mongo.Collection('comments');

//Meteor methods should always be defined in common code
Meteor.methods({



});


Comments.schema = new SimpleSchema({
  content: {type: String},
  likes: {type: Number , defaultValue: 0},

})
