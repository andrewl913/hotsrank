// import all client startup
import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from 'angular-ui-router';
import ngRoute from 'angular-route';
import angularBootstrap from 'angular-ui-bootstrap'
import angularAnimate from 'angular-animate'


//import controllers here
import heroesController from '../../ui/components/heroesComponent/heroesController.js'
import appController from '../../ui/components/appComponent/appController.js'
import commentController from '../../ui/components/commentComponent/commentController.js'
import userController from '../../ui/components/userComponent/userController.js'
import teamController from '../../ui/components/teamComponent/teamController.js'
import heroSearchBarController from '../../ui/components/heroSearchBarComponent/heroSearchBarController.js'
import heroClassSelectorController from '../../ui/components/heroClassSelectorComponent/heroClassSelectorController.js'
import battlegroundsController from '../../ui/components/battlegroundsComponent/battlegroundsController.js'
import heroPortraitController from '../../ui/components/heroPortraitComponent/heroPortraitController.js'
import bgThumbnailController from '../../ui/components/bgThumbnailComponent/bgThumbnailController.js'
import bgDetailController from '../../ui/components/bgDetailComponent/bgDetailController.js'
//end controllers

// use the angularMeteor as a module the hotsRank app will use
var app = angular.module('hotsRank', [angularMeteor, ngRoute, angularBootstrap, angularAnimate, 'accounts.ui']);

const componentsPath = 'imports/ui/components/'

//components to the app here. DO NOT CHANGE!
app.component('heroes', {
  controller: heroesController,
  templateUrl: componentsPath + 'heroesComponent/heroes.html'
});

app.component('app', {
  controller: appController,
  templateUrl: componentsPath + 'appComponent/app.html'
});

app.component('comment', {
  controller: commentController,
  templateUrl: componentsPath + 'commentComponent/comment.html'
})

app.component('user', {
  controller: userController,
  templateUrl: componentsPath + 'userComponent/user.html'
})

app.component('team', {
  controller: teamController,
  templateUrl: componentsPath + 'teamComponent/team.html'
})

app.component('heroSearchBar', {
  bindings: {
    searchParams: '=',
  },
  controller: heroSearchBarController,
  templateUrl: componentsPath + 'heroSearchBarComponent/heroSearchBar.html'
})

app.component('heroClassSelector', {
  bindings: {
    role: '='
  },
  controller: heroClassSelectorController,
  templateUrl: componentsPath + 'heroClassSelectorComponent/heroClassSelector.html'
})

app.component('battlegrounds', {
  controller: battlegroundsController,
  templateUrl: componentsPath + 'battlegroundsComponent/battlegrounds.html'
})

app.component('heroPortrait', {
  bindings: {
    hero: '=',
    heroCheckCount: '='
  },
  require: {
    parent: '^heroes'
  },
  controller: heroPortraitController,
  templateUrl: componentsPath + 'heroPortraitComponent/heroPortrait.html'
})

app.component('bgThumbnail', {
  bindings: {
    thumbnail: '=',
    bgname: '=',
    bgurl: '='
  },
  controller: bgThumbnailController,
  templateUrl: componentsPath + 'bgThumbnailComponent/bgThumbnail.html'
})

app.component('bgDetail', {
  controller: bgDetailController,
  templateUrl: componentsPath + 'bgDetailComponent/bgDetail.html'
})

//end components

export default app
